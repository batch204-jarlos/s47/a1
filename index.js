// Directions:
// Create two event listeners for when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right


// Stretch goal: Instead of an anonymous function, create a new function that the two event listeners will call

// Guide question: Where do the names come from and where should they go?

let input1 = document.querySelector('#txt-first-name');
let input2 = document.querySelector('#txt-last-name');
let span1 = document.querySelector('#span-full-name');
let span2 = document.querySelector('#span2-full-name');


// span1.addEventListener('keyup', (e) => {
// 	// (e.target.value) = document.getElementById('txt-first-name').innerHTML
// 	input1.addEventListener(e.target.value)
// })

input1.addEventListener('keyup', (e) => {
	span1.innerHTML = event.target.value;
	myfunc1();
})

input2.addEventListener('keyup', (f) => {
	span2.innerHTML = event.target.value;
	myfunc2();
})


function myfunc1() {
	span1.innerHTML = event.target.value;
}

function myfunc2() {
	span2.innerHTML = event.target.value;
}